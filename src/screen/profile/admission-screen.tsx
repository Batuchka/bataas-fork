import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { Box, Text, Spacing, Avatar, Border, Queue, Button } from '../../components';
import { LeftArrowIcon } from '../../components/icons';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params'

 
export const AdmissionScreen = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView>
      <Box height="100%" width="100%" role="white">
        <Box height={100} justifyContent="center"> 
          <Queue>
            <Button category="text" status="active" onPress={() => navigation.navigate(NavigationRoutes.Profile)}>
              <LeftArrowIcon role="black"/>
            </Button>
            <Text type="title3" bold role="black" textAlign="center">Элсэлтийн явц</Text>
          </Queue>
        </Box>
        <ScrollView>

        </ScrollView>
      </Box>
    </SafeAreaView>
)};