import React from 'react'
import { SafeAreaView } from 'react-native';
import { Box, Text, Spacing, Avatar, Border, Queue, Button } from '../../components';
import { LogOut, ProfileIcon, DocumentIcon, RightArrowIcon } from '../../components/icons';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params'
 
export const ProfileScreen = () => {
  const user = false;
  const navigation = useNavigation();
  if(!user) {
    navigation.navigate(NavigationRoutes.ProfileNotLoggedIn)
  }
  return (
    <SafeAreaView>
      <Box height="100%" width="100%" role="offwhite">
        <Box position="absolute" right={18} top={20}>
          <LogOut/>
        </Box>
        <Box flex={1} alignItems="center" justifyContent="flex-end">
          <Text type="title3" textAlign="center" role="black" bold>Profile</Text>
          <Spacing mt={8} mb={4}>
            <Avatar 
              size="M"
              url="https://s3-alpha-sig.figma.com/img/c7af/8aa6/977d5fe6d40c141f8b4f69310dce6e08?Expires=1619395200&Signature=HI~-tMoGAWtePufH9VlVS1EdPkEQLTabAohvrNJdffFFWYcEZADhN0zciJS~lHHw4wQ-Je9ELG0AwXuO6gWl1CpPbFnIsXaCFZivo5t0-XqODXDpyi5nwRXppdZJ6tJ59y7J5cKWJtLhx~zZ-qZF7LQw5tBiviQWIjOO1BTafZRPjQMMw3DDMNroK7JVqfx9brB3h3gImZ4GJCR7KYq0xeuh~BBTArzhKb6WhshIyWPfECWRSFEHWcvNQo8nb2cDsGzUNUyz-tWzfANYgB9ImIqc7sm0xbTmdqevznE89pbotEa9NkxR-wt25K24C6GM-m7A2ZcEXcfwvUHLm7C6wg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
            />
          </Spacing>
          <Text type="title3" role="black" textAlign="center" bold>М.Ганбат</Text>
        </Box>
        <Box flex={2}>
          <Spacing mt={3} pt={8}>
            <Border topWidth={1} bottomWidth={1} role="primary300">
                <Box height={64} justifyContent="center">
                  <Spacing mh={5}>
                    <Queue justifyContent="space-between">
                      <Queue>
                        <ProfileIcon role="black" width={24} height={24}/>
                        <Spacing ml={4}>
                          <Text type="body" role="black">Хубийн мэдээлэл</Text>
                        </Spacing>
                      </Queue>
                      <Box position="absolute" right={5} bottom={-10} >
                        <Button category="text" status="active" height={64} onPress={()=> navigation.navigate(NavigationRoutes.PersonalInformation)}>
                          <RightArrowIcon role="black"/>
                        </Button>
                      </Box>
                    </Queue>
                  </Spacing>
                </Box>
            </Border>
          </Spacing>
            <Border bottomWidth={1} role="primary300">
                <Box height={64} justifyContent="center">
                  <Spacing mh={5}>
                    <Queue justifyContent="space-between">
                      <Queue>
                        <DocumentIcon/>
                        <Spacing ml={4}>
                          <Text type="body" role="black">Элсэлтийн явц</Text>
                        </Spacing>
                      </Queue>
                      <Box position="absolute" right={5} bottom={-10} >
                        <Button category="text" status="active" height={64} onPress={()=> navigation.navigate(NavigationRoutes.Admission)}>
                          <RightArrowIcon role="black"/>
                        </Button>
                      </Box>
                    </Queue>
                  </Spacing>
                </Box>
            </Border>
        </Box>
      </Box>
    </SafeAreaView>
)}
