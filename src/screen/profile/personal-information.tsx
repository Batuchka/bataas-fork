import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { Box, Text, Spacing, Avatar, Border, Queue, Button } from '../../components';
import { LeftArrowIcon } from '../../components/icons';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params'

 
export const PersonalInformationScreen = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView>
      <Box height="100%" width="100%">
        <Box height={100} justifyContent="center"> 
          <Queue>
            <Box position="absolute" left={20} top={-12}>
              <Button category="text" status="active" size="l" onPress={() => navigation.navigate(NavigationRoutes.Profile)}>
                <LeftArrowIcon role="black"/>
              </Button>
            </Box>
            <Text type="title3" bold textAlign="center" role='black'>Хубийн мэдээлэл</Text>
          </Queue>
        </Box>
        <ScrollView>

        </ScrollView>
      </Box>
    </SafeAreaView>
)};