import React from 'react';
import { SafeAreaView, TouchableOpacity } from 'react-native';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { CloseLineIcon, ShareIcon } from '../../components/icons'
import { AdmissionProcessCard, AdmissionProcessCardContent, AdmissionProcessCardHeader, Button, CheckCircleIllustration, Text } from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const RegistrationSuccessScreen: React.FC<RegistrationSuccessScreenType> = ({ route }) => {
    const { onClick } = route.params || {}
    const navigation = useNavigation();

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Box>
                <Spacing m={3}>
                    <Queue justifyContent={'flex-end'}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <CloseLineIcon width={20} height={20}></CloseLineIcon>
                        </TouchableOpacity>
                    </Queue>
                    <Spacing mt={6} ml={7} mr={7}>
                        <Stack alignItems={'center'} size={2}>
                            <CheckCircleIllustration />
                            <Text textAlign={'center'} bold type={'title3'} fontFamily={'Montserrat'}>Бүртгэл амжилттай</Text>
                            <Text textAlign={'center'} type={'body'}>Та HOPE хөтөлбөрт амжилттай бүртгүүллээ</Text>
                        </Stack>
                    </Spacing>

                </Spacing>
            </Box>
            <Spacing m={4}>
                <Stack size={4}>
                    <AdmissionProcessCard>
                        <AdmissionProcessCardHeader>
                            <Text bold type={'headline'}>Шалгалт өгөх</Text>
                        </AdmissionProcessCardHeader>
                        <AdmissionProcessCardContent>
                            <Spacing m={3}>
                                <Stack size={4}>
                                    <Text type={'footnote'}>Та шалгалтаа одоо өгч болно. Шалгалтаа одоо өгөх дараа өгөх ямар ч ялгаа байхгүй</Text>
                                    <Button onPress={onClick} width={'100%'}>
                                        <Text type={'headline'} role={'primary100'} bold fontFamily={'Montserrat'}>Товчлуур</Text>
                                    </Button>
                                </Stack>
                            </Spacing>
                        </AdmissionProcessCardContent>
                    </AdmissionProcessCard>
                    <AdmissionProcessCard>
                        <AdmissionProcessCardHeader>
                            <Text bold type={'headline'}>Холбоос дамжуулах</Text>
                        </AdmissionProcessCardHeader>
                        <AdmissionProcessCardContent>
                            <Spacing m={3}>
                                <Stack size={4}>
                                    <Text type={'footnote'}>Элсэгч хаанаас ч энэ холбоосийг ашиглаж шалгалтаа өгч болно</Text>
                                    <Button
                                        onPress={() => console.log('share')}
                                        category={'ghost'}
                                        width={'100%'}>
                                        <Queue alignItems={'center'}>
                                            <ShareIcon></ShareIcon>
                                            <Spacing ml={1} />
                                            <Text type={'headline'} role={'primary500'} bold fontFamily={'Montserrat'}>Share</Text>
                                        </Queue>
                                    </Button>
                                </Stack>
                            </Spacing>
                        </AdmissionProcessCardContent>
                    </AdmissionProcessCard>
                </Stack>
            </Spacing>
        </SafeAreaView>
    );

};

type RegistrationSuccessScreenType = {
    route: any,
};