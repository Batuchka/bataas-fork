export * from './home';
export * from './test';
export * from './profile';
export * from './splash';
export * from './exam';
export * from './internet-failed-screen';
export * from './registration-success-screen';
export * from './courses';
