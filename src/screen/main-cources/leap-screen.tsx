import React from 'react';
import { ScrollView, Dimensions } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { CourcesTab } from '../../navigation/top-nav'

import {
    LeapImage,
    Box,
    Button,
    Text
} from '../../components';


export const LeapScreen: React.FC<any> = ({ tabs, y }) => {

    return (
        <SafeAreaView>
            <ScrollView
                onResponderMove={() => { console.log('outer responding'); }}
            >
                <Box
                    role='primary500'
                    width={"100%"}
                    height={280}
                    justifyContent={'center'}
                    alignItems={'center'}
                >
                    <LeapImage />
                </Box>
                <CourcesTab />
            </ScrollView>
            <Box bottom={80} position={'absolute'} width={'90%'} alignSelf={'center'} >
                <Button size={'l'} width={'100%'} onPress={() => { '' }} >
                    <Text fontFamily={"Montserrat"} type={'callout'} role={'white'} bold >
                        БҮРТГҮҮЛЭХ
                    </Text>
                </Button>
            </Box>
        </SafeAreaView>
    )
}
