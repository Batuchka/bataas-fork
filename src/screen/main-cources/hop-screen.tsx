import React, { useState } from 'react';
import { ScrollView, Animated, FlatList, StyleSheet, Dimensions } from 'react-native'
import { interpolate } from 'react-native-reanimated'
import { SafeAreaView } from 'react-native-safe-area-context'
import { CourcesTab } from '../../navigation/top-nav'
import { useNavigation } from '@react-navigation/native'
import { PhotoCard } from './photo-card'

const { width, height } = Dimensions.get('screen');
const duuk = require('../../assets/images/duuk-bagsh.png');

import {
    Text,
    Spacing,
    HopImage,
    Box,
    ClockIcon,
    ThumbIcon,
    PriceIcon,
    Stack,
    Border,
    DesignIcon,
    Button,
    ExpandableText
} from '../../components';
import { ClassToggle } from '../../navigation/class-toggle';
import { ImplementAdmissionProcess } from '../../components/core/implement-admission-process';

const data = [
    {
        source: duuk,
        name: 'Э. Дөлгөөн',
        icon: DesignIcon,
        title: 'Дизайн',
        id: 1
    },
    {
        source: duuk,
        name: 'Э. Дөлгөөн',
        icon: DesignIcon,
        title: 'Дизайн',
        id: 2
    },
    {
        source: duuk,
        name: 'Э. Дөлгөөн',
        icon: DesignIcon,
        title: 'Дизайн',
        id: 3
    }
]

export const HopScreen: React.FC<any> = ({ tabs, y }) => {

    return (
        <SafeAreaView>
            <ScrollView
                onResponderMove={() => { console.log('outer responding'); }}
            >
                <Box
                    role="caution600"
                    width={"100%"}
                    height={280}
                    justifyContent={'center'}
                    alignItems={'center'}
                >
                    <HopImage />
                </Box>
                <CourcesTab />
            </ScrollView>
            <Box bottom={80} position={'absolute'} width={'90%'} alignSelf={'center'} >
                <Button size={'l'} width={'100%'} onPress={() => { '' }} >
                    <Text fontFamily={"Montserrat"} type={'callout'} role={'white'} bold >
                        БҮРТГҮҮЛЭХ
                    </Text>
                </Button>
            </Box>
        </SafeAreaView>
    )
}

export const AboutScreen = () => {

    return (
        <Spacing p={5}>
            
            <Box >
                <Text fontFamily={'Montserrat'} bold type={'body'} >
                    Давуу талууд
                    </Text>
                <Spacing mt={8} >
                    <Stack size={6} >
                        <Box flexDirection={'row'} flex={1} justifyContent={'space-between'} alignItems={'center'}>
                            <ThumbIcon />
                            <Box width={'auto'}>
                                <Text bold type={'body'} numberOfLines={3} >
                                    Чадварлаг инженер
                                    </Text>
                                <Text numberOfLines={3} width={263} >
                                    Сүүлийн үеийн гар утасны аппликэшн болон вэб хуудас хийх чадвартай болж төгсөнө.
                                    </Text>
                            </Box>
                        </Box>
                        <Box flexDirection={'row'} flex={1} justifyContent={'space-between'} alignItems={'center'}>
                            <PriceIcon />
                            <Box width={'auto'}>
                                <Text bold type={'body'} numberOfLines={3} >
                                    Боломжийн үнэ
                                    </Text>
                                <Text numberOfLines={3} width={263} >
                                    Дэлхийд үнлэгдэх мэдлэг, чадварийг боломжийн үнээр суралцах боломж.
                                    </Text>
                            </Box>
                        </Box>
                        <Box flexDirection={'row'} flex={1} justifyContent={'space-between'} alignItems={'center'}>
                            <ClockIcon />
                            <Box width={'auto'}>
                                <Text bold type={'body'} numberOfLines={3} >
                                    Цаг хэмнэх
                                    </Text>
                                <Text numberOfLines={3} width={263} >
                                    Дэлхийд үнлэгдэх мэдлэг, чадварийг маш богино хугацаанд чанартай сурна.
                                    </Text>
                            </Box>
                        </Box>
                    </Stack>
                </Spacing>
            </Box>
            <Spacing mt={8} mb={8}>
                <Border lineWidth={1} role='primary200' />
            </Spacing>
            <ExpandableText/>
            <Spacing mt={8} mb={8}>
                <Border lineWidth={1} role='primary200' />
            </Spacing>
            <Box>
                <Text fontFamily={'Montserrat'} bold type={'body'} >
                    Цаг хувиарлалт
                    </Text>
                <Spacing mt={8} >
                    <Box>
                        <ClassToggle />
                    </Box>
                </Spacing>
            </Box>
            <Spacing mt={8} mb={8}>
                <Border lineWidth={1} role='primary200' />
            </Spacing>
            <Box>
                <FlatList
                    data={data}
                    renderItem={({ item }) => (
                        <PhotoCard
                            source={item.source}
                            title={item.title}
                            name={item.name}
                            width={264}
                            height={320}
                            icon={<DesignIcon />}
                        />
                    )}
                    keyExtractor={(item) => item.id}
                    ItemSeparatorComponent={() => <Box width={16} />}
                    showsHorizontalScrollIndicator={false}
                    horizontal
                />
                <Spacing mt={4} />
                <Text bold type={'body'} >
                    Дэлгэрэнгүй
                    </Text>
                <Spacing mt={2} />
                <Text>
                    Энэ чиглэлээр 4-өөс дээш жил ажилласан туршлагатай, Вэб, Мобайл програмын дизайн, хөгжүүлэлтэд анхаарч ажилладаг бүтээгдэхүүний дизайнер
                    </Text>
            </Box>
            <Spacing mt={8} mb={8}>
                <Border lineWidth={1} role='primary200' />
            </Spacing>
            <Box>
                <Text fontFamily={'Montserrat'} bold type={'body'} >
                    Элсэлтийн явц
                    </Text>
                <Spacing m={4} />
                <ImplementAdmissionProcess />
            </Box>
            <Spacing m={15} />
        </Spacing>
    )
}

export const ProgramScreen = () => {
    return (
        <SafeAreaView>
            <Text>
            </Text>
        </SafeAreaView>
    )
}

export const WorksScreen = () => {
    return (
        <SafeAreaView>
            <Text>
                lol
            </Text>
        </SafeAreaView>
    )
}