import React from 'react'
import { Image, StyleSheet, ScrollView } from 'react-native'
import { Border, Shadow, Text, Button } from '../../components/core'
import { Spacing, Box, Stack, Queue } from '../../components/layout'
import { CheckCircle } from '../../components/icons'

export const FCAboutScreen = () => {

    return (
        <ScrollView>
            <Box role = {'white'}>
                <Box width={'100%'} justifyContent={'center'} role={'white'}>
                    <Spacing pt={6} pb={6} pl={4} pr={4}>
                        <Stack size={4}>
                            <Text type={'headline'} bold={true} role={'primary500'}>Суурь дизайний онол</Text>
                            <Box flexDirection={'column'}>
                                <Image source={require('../../assets/images/dulguun-bagsh.png')} style={styles.avatar} />

                                <Shadow h={2} w={0} radius={4} opacity={0.15} role={'primary500'}>
                                    <Shadow radius={2} h={0} w={0} opacity={0.25} role={'primary500'}>
                                        <Border radius={4}>
                                            <Spacing pl={10}>
                                                <Box role={'white'} alignItems={'center'} height={64} width={'100%'} justifyContent={'center'}>
                                                    <Box width={'auto'} height={'auto'}>
                                                        <Text type={'headline'} bold={true} role={'primary500'}>Э. Дөлгөөнуурынжав</Text>
                                                        <Text type={'caption1'} bold={true} role={'primary400'}>Мастер багш</Text>
                                                    </Box>
                                                </Box>
                                            </Spacing>
                                        </Border>
                                    </Shadow>
                                </Shadow>
                            </Box>
                        </Stack>
                    </Spacing>
                </Box>
                <Box height={1} width={'100%'} role={'offwhite'}></Box>

                    <Spacing pr={4} pl={4} pt={6} pb={6}>
                        <Box width={'100%'} height={'auto'} role={'white'}>
                            <Stack size={4}>
                                <Text type={'headline'} bold={true} role={'primary500'}>Хичээлиийн танилцуулга</Text>
                                <Text type={'body'} bold={false} role={'gray'}>Graphic design is all around us, in a myriad of forms, both on screen and in print, yet it is always made up of images and words to create a communication goal.</Text>
                            </Stack>
                        </Box>
                    </Spacing>

                <Box height={1} width={'100%'} role={'offwhite'}></Box>

                
                    <Spacing pr={4} pl={4} pt={6} pb={6}>
                        <Box width={'100%'} height={'auto'} role={'white'}>
                            <Stack size={4}>
                                <Text type={'headline'} bold={true} role={'primary500'}>Сургалтаас олж авах мэдлэг</Text>
                                <Box>
                                    <Stack size={2}>
                                        <Box flexDirection={'row'}>
                                            <Queue size={2}>
                                                <CheckCircle width={24} height={24} />
                                                <Text type={'body'} role={'gray'}>Дизайний эхний үндсэн ойлголт</Text>
                                            </Queue>
                                        </Box>
                                        <Box flexDirection={'row'}>
                                            <Queue size={2}>
                                                <CheckCircle width={24} height={24} />
                                                <Text type={'body'} role={'gray'}>Дизайний төрөл болон салбарууд</Text>
                                            </Queue>
                                        </Box>
                                        <Box flexDirection={'row'}>
                                            <Queue size={2}>
                                                <CheckCircle width={24} height={24} />
                                                <Text type={'body'} role={'gray'}>Чиглэлээ хэрхэн тодорхойлох</Text>
                                            </Queue>
                                        </Box>
                                        <Box flexDirection={'row'}>
                                            <Queue size={2}>
                                                <CheckCircle width={24} height={24} />
                                                <Text type={'body'} role={'gray'}>Онолын мэдлэг</Text>
                                            </Queue>
                                        </Box>
                                        <Box flexDirection={'row'}>
                                            <Queue size={2}>
                                                <CheckCircle width={24} height={24} />
                                                <Text type={'body'} role={'gray'}>Анхан шатны дизайн гаргах</Text>
                                            </Queue>
                                        </Box>
                                        <Box flexDirection={'row'}>
                                            <Queue size={2}>
                                                <CheckCircle width={24} height={24} />
                                                <Text type={'body'} role={'gray'}>Usage of design tools</Text>
                                            </Queue>
                                        </Box>
                                    </Stack>
                                </Box>
                            </Stack>
                        </Box>
                    </Spacing>
                <Box height={1} width={'100%'} role={'offwhite'}></Box>

                    <Spacing pl={4} pr={4} pt={6} pb={6}>
                        <Box height={'auto'} width={'100%'}>
                            <Stack size={4}>
                                <Text type={'headline'} bold={true} role={'primary500'}>Хөтөлбөрт хичээлд бүртгүүлэх</Text>
                                <Text type={'body'} bold={false} role={'gray'}>Хэрвээ та илүү өргөн хүрээтэй хичээл, 4 жилийн чадварлаг багш нараар ахлуулсан хичээлд хамрагдах бол доорх товч дээр дарж өөрийн мэдээллээ оруулж бидэнтэй нэгдээрэй.</Text>
                                <Button>
                                    <Text fontFamily={'Montserrat'} bold={true} role={'white'} type={'subheading'}>Бүртгүүлэх</Text>
                                </Button>
                            </Stack>
                        </Box>
                    </Spacing>
            </Box>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    avatar: {
        width: 64,
        height: 70,
        borderRadius: 50,
        zIndex: 50,
        position: 'absolute'
    }
})