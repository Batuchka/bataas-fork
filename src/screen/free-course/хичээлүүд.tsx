import React, {useState} from 'react'
import { Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import { Border, Shadow, Text, Button } from '../../components/core'
import { Spacing, Box, Stack, Queue } from '../../components/layout'
import { PlayButtonFreeCourse, LockedButtonFreeCourse } from '../../components/icons'
import { DropDown } from '../../components'
import { shouldUseActivityState } from 'react-native-screens'

export const FCStudyScreenComponent: React.FC<any> = ({ name, image, button, progressBar }) => {
    return (
        <Box width={'100%'} height={'auto'}>
            <Shadow h={2} w={0} radius={4} opacity={0.15} role={'primary500'}>
                <Shadow radius={2} h={0} w={0} opacity={0.25} role={'primary500'}>
                    <Border radius={8}>
                        <Box width={'100%'} role={'white'}>
                            <Spacing pl={4} pt={4} pb={4} pr={4}>
                                <Box width={'100%'} height={'auto'} flexDirection={'row'} alignItems={'center'}>
                                    <Queue size={4}>
                                        <Image source={image} style={styles.img} />
                                        <Box justifyContent={'space-around'} width={'64%'}>

                                            <Text type={'headline'} role={'primary500'} bold={true}>{name}</Text>
                                            <Text type={'caption1'} role={'gray'}>Хичээл 2</Text>
                                            <Border radius={12}>
                                                <Box width={'75%'} role={'offwhite'} height={4}>
                                                    <Box width={progressBar} role={'success400'} height={4}></Box>
                                                </Box>
                                            </Border>
                                        </Box>
                                    </Queue>
                                    <TouchableOpacity>
                                        {
                                            button === 'locked' ?
                                                <LockedButtonFreeCourse width={40} height={40} />
                                                :
                                                <PlayButtonFreeCourse width={40} height={40} />
                                        }
                                    </TouchableOpacity>
                                </Box>
                            </Spacing>
                        </Box>
                    </Border>
                </Shadow>
            </Shadow>
        </Box>

    )
}
export const FCStudyScreen = () => {

    const [contents, setContents] = useState(['dfdsf', 'fdsfdsfsd'])
    return (
        <ScrollView>
            <Spacing pt={9} pb={6} pl={4} pr={4}>
                <Stack size={4}>
                    <Text type={'headline'} role={'primary500'} bold={true}>Эхлэл хичээл</Text>

                    <FCStudyScreenComponent name={'Userflow'} progressBar={'50%'} image={require('../../assets/images/userflow.png')} />
                    <FCStudyScreenComponent name={'Userflow'} progressBar={'25%'} image={require('../../assets/images/userflow2.png')} />
                    <FCStudyScreenComponent name={'Sitemap'} image={require('../../assets/images/sitemap.png')} />

                    <Spacing pt={2} pb={2}>
                        <Text type={'headline'} role={'primary500'} bold={true}>Дадлагат хичээл</Text>
                    </Spacing>

                    <FCStudyScreenComponent button={'locked'} name={'Mockup'} image={require('../../assets/images/mockup.png')} />
                    <FCStudyScreenComponent button={'locked'} name={'Mockup'} image={require('../../assets/images/mockup2.png')} />
                    <FCStudyScreenComponent button={'locked'} name={'Mockup'} image={require('../../assets/images/mockup3.png')} />
                    <FCStudyScreenComponent button={'locked'} name={'Mockup'} image={require('../../assets/images/mockup4.png')} />
                </Stack>
            </Spacing>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    img: {
        width: 64,
        height: 64
    }
})