import React, { useContext, useState } from 'react';
import { SafeAreaView } from 'react-native';
import { Spacing } from '../../../components';
import { Text } from '../../../components/core';
import { Input } from '../../../components/inputs';
import { Box } from '../../../components/layout/box';
import { AuthContext } from '../../../provider/auth';
export const Auth1 = ({title}: any) => {
    const { setnumber } = useContext(AuthContext)
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }} >
            <Spacing mt={14}>
                <Box height={44} justifyContent='center' alignItems='center'>
                    <Text height={44}  bold textAlign='center' type='headline'>
                        {title}
                    </Text>
                </Box>
            </Spacing>
            <Spacing mt={12} mb={22}>
                <Box height={56} justifyContent='center' alignItems='center' >
                    <Input
                        placeholder={'Утасны дугаар'}
                        type={'default'}
                        onChangeText={(text: string) => setnumber(text)}
                    />
                </Box>
            </Spacing>
        </SafeAreaView>
    );
};
