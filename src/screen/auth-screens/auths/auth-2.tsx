import React from 'react';
import { SafeAreaView } from 'react-native';
import { DigitInput, Spacing } from '../../../components';
import { Text } from '../../../components/core';
import { Box } from '../../../components/layout/box';
export const Auth2 = () => {
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }} >
            <Spacing mt={14}>
                <Box height={44} justifyContent='center' alignItems='center'>
                    <Text height={44} bold textAlign='center' width={185} type='headline'>
                        Гар утасны дугаараа оруулна уу
                    </Text>
                </Box>
            </Spacing>
            <Spacing mt={12} mb={22}>
                <Box height={56} justifyContent='center' alignItems='center' >
                    <DigitInput />
                </Box>
            </Spacing>
        </SafeAreaView>
    );
};
