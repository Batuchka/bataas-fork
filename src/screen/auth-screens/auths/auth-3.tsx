import firestore from '@react-native-firebase/firestore';
import React, { useContext, useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Spacing, LoadingCircle, DigitInput } from '../../../components';
import { Text, Button } from '../../../components/core';
import { Input } from '../../../components/inputs';
import { Box } from '../../../components/layout/box';
import { AuthContext } from '../../../provider/auth';
import { ProgressBar } from '../../../components/progress-bar'
import { identity } from 'lodash';
const styles = StyleSheet.create({})
export const Auth3 = ({setcred  , cred }: any) => {
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }} >
            <Spacing mt={14}>
                <Box height={44} justifyContent='center' alignItems='center'>
                    <Text height={44} bold textAlign='center' width={185} type='headline'>
                        Гар утасны дугаараа оруулна уу
                    </Text>
                </Box>
            </Spacing>
            <Spacing mt={12} mb={22}>
                <Box height={56} justifyContent='center' alignItems='center' >
                        <Box height={128} alignItems='center' justifyContent='center'>
                            <Input
                                placeholder={'Овог Нэр'}
                                type={'default'}
                                onChangeText={(text: string) => setcred({...cred, lastname:text})}
                            />
                            <Input
                                placeholder={'Нэр'}
                                type={'default'}
                                onChangeText={(text: string) => setcred({...cred, firstname:text})}
                            />
                        </Box>
                </Box>
            </Spacing>
        </SafeAreaView>
    );
};
