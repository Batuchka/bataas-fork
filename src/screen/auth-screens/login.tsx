import React, { useContext } from 'react';
import { SafeAreaView } from 'react-native';
import { ProgressStep, ProgressSteps } from '../../components/';
import { AuthContext } from '../../provider/auth';
import { Auth1, Auth2, Auth3 } from './auths/index';

export const Login = ({ navigation }: any) => {
    const { user, signInWithPhoneNumber, confirmCode } = useContext(AuthContext)
    const section1 = async () => {
        await signInWithPhoneNumber();
    }
    const section2 = async () => {
        await confirmCode();
    }
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }} >
            <ProgressSteps lastButtonFunction={() => { section2() }}>
                <ProgressStep label='Login1' id='Login1' onNext={() => { section1() }}>
                    <Auth1  title='Гар утасны дугаараа оруулна уу Login'/>
                </ProgressStep>
                <ProgressStep label='Login2' id='Login2'>
                    <Auth2 />
                </ProgressStep>
            </ProgressSteps>
        </SafeAreaView>
    )
}