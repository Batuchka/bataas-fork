import React, { useContext, useState } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { ProgressSteps, ProgressStep, progressStepsContext } from '../../components'
import { AuthContext } from '../../provider/auth';
import { Auth1, Auth2, Auth3 } from './auths/index'
import firestore from '@react-native-firebase/firestore'
const styles = StyleSheet.create({})
export const Signup = () => {
    const { user, signInWithPhoneNumber, confirmCode } = useContext(AuthContext)
    const [cred, setcred] = useState({
        firstname: '',
        lastname: '',
    })
    const section1 = async () => {
        await signInWithPhoneNumber();
    }
    const section2 = async () => {
        await confirmCode();
    }
    const section4 = () => {
        if (firestore) {
            console.log('created')
            firestore().collection('users').doc(user && user.uid).set(cred);
        }
    }
    console.log(cred)
    return (
        <SafeAreaView style={{ backgroundColor: 'white', flex: 1 }} >
            <ProgressSteps lastButtonFunction={() => { section4() }}>
                <ProgressStep label='Auth1' id='Auth1' onNext={() => { section1() }}>
                    <Auth1 title=' Гар утасны дугаараа оруулна уу' />
                </ProgressStep>
                <ProgressStep label='Auth2' id='Auth2' onNext={() => { section2() }}>
                    <Auth2 />
                </ProgressStep>
                <ProgressStep label='Auth3' id='Auth3' >
                    <Auth3 setcred={setcred}  />
                </ProgressStep>
            </ProgressSteps>
        </SafeAreaView>
    );
};
