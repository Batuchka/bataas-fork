import React, { useState } from 'react';
import { SafeAreaView, View } from 'react-native';
import { Text, Box, Spacing, SuccessIllustration, SuccessBirdIllustration, RightArrowIcon, CloseLineIcon, SuccessNextIllustration, Button } from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler';


export const SuccessScreen: React.FC<any> = ({ route }) => {
    const { successMessage } = route.params || {};
    const navigation = useNavigation();

    return (
        <Box flex={1} role = {'fawhite'}>
            <SafeAreaView style={{ flex: 1 }}>
                
                <Box flex={1} justifyContent={'center'} alignItems={'center'}>
                    <Spacing ph={4} pv={40} >
                        <Box position={'relative'} flex={1} top={50} width={300} role={'white'} justifyContent={'center'} alignItems={'center'}>
                            <Box position={'absolute'} top={-70} zIndex={10}>
                                <SuccessIllustration />
                            </Box>

                            <Box position={'absolute'} top={150} alignItems={'center'} zIndex={10}>
                            <Text bold fontFamily={'Montserrat'} width={'auto'} type={'largeTitle'}>Амжилттай</Text>
                                <Text width={'auto'} type={'footnote'}>{ successMessage }</Text>
                            </Box>

                            <Box position={'absolute'} top={380} right={20} zIndex={10}>
                                <TouchableOpacity onPress={() => navigation.navigate(NavigationRoutes.HomeScreen)}>
                                    <SuccessNextIllustration />
                                </TouchableOpacity>
                            </Box>
                           
                        </Box>
                    </Spacing>
                </Box>

                <Box position={'absolute'} bottom={20}>
                    <SuccessBirdIllustration />
                </Box>

            </SafeAreaView>
        </Box>
    );
};