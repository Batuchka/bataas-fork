import React from 'react';
import { FlatList, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { Border, Box, Spacing, Text, NestLogo, BackgroundImage, Button, RightArrowIcon, ScrollCard, LessonDashboard, Stack, Queue } from '../../components';
const img = 'https://proforientator.ru/publications/articles/New%20Folder/programmist_st.jpg'
const hop = require('../../assets/images/hop.png');
const leap = require('../../assets/images/leap.png');

const cardsData = [
    {
        id: 'card-0',
        title: 'DESIGN',
        description: 'UI UX design fundamentals',
        source: img,
    },
    {
        id: 'card-1',
        title: 'DESIGN',
        description: 'UI UX design fundamentals',
        source: img,
    },
    {
        id: 'card-2',
        title: 'DESIGN',
        description: 'UI UX design fundamentals',
        source: img,
    },
    {
        id: 'card-3',
        title: 'DESIGN',
        description: 'UI UX design fundamentals',
        source: img,
    },
];

export const CourseScreen = () => {
    const styles = StyleSheet.create({
        scroll: {
            height: '100%',
        }
    })
    return (
        <Box>
            <Box height={116} top={0} justifyContent='center' alignItems='center' width='100%' role='white'>
                <Box height={44} />
                <Box width='90%' height={42} flexDirection='row' justifyContent='space-between' alignItems='center'>
                    <NestLogo />
                    <Border radius={16}>
                        <Box width={32} height={32} role='primary500' />
                    </Border>
                </Box>
            </Box>
            <ScrollView>
                <Box width='100%' height={220} justifyContent='center' alignItems='center'>
                    <Border radius={8}>
                        <Box role='white' width={350} height={173}>
                            <Spacing ph={4} pv={4}>
                                <Box justifyContent='space-between' height={140}>
                                    <Text type='title3' fontFamily='Montserrat' role='primary500' bold={true}>Илүү ихийг үз</Text>
                                    <Text type='subheading' role='primary400' width={312} numberOfLines={2}>Та манай аппликэшинд бүртгүүлснээр цааш илүү их хичээлүүдийг үзэх боломжтой болно. </Text>
                                    <Button onPress={() => { }} category='fill' size='s' width={311} >
                                        <Box flexDirection='row' justifyContent='center' alignItems='center'>
                                            <Box>
                                                <Text type='callout' role='white' bold={true} fontFamily='Montserrat'>Бүртгүүлэх</Text>
                                            </Box>
                                            <Box width={40} justifyContent='center' alignItems='center'>
                                                <RightArrowIcon />
                                            </Box>
                                        </Box>
                                    </Button>
                                </Box>
                            </Spacing>
                        </Box>
                    </Border>
                </Box>
                <Box height='auto' width='100%' justifyContent='space-evenly'>
                    <Spacing ph={4}>
                        <Text type='headline' role='black' bold> ТУРШИЛТЫН ХИЧЭЭЛ </Text>
                        <Spacing pv={4}>
                            <FlatList
                                data={cardsData}
                                renderItem={({ item }) => (
                                    <ScrollCard
                                        source={item.source}
                                        title={item.title}
                                        description={item.description}
                                    />
                                )}
                                keyExtractor={(item) => item.id}
                                ItemSeparatorComponent={() => <Box width={16} />}
                                showsHorizontalScrollIndicator={false}
                                horizontal
                            />
                        </Spacing>
                        <Spacing pt={4}>
                            <Text type='headline' role='black' bold> ҮНДСЭН ХӨТӨЛБӨР </Text>
                        </Spacing>
                        <Spacing pv={4}>
                            <Box flexDirection='row' width='100%' justifyContent='space-between'>
                                <Border radius={8}>
                                    <TouchableOpacity>
                                        <BackgroundImage
                                            source={hop}
                                            height={170}
                                            width={170}
                                        ></BackgroundImage>
                                    </TouchableOpacity>
                                </Border>
                                <Border radius={8}>
                                    <TouchableOpacity>
                                        <BackgroundImage
                                            source={leap}
                                            height={170}
                                            width={170}
                                        ></BackgroundImage>
                                    </TouchableOpacity>
                                </Border>
                            </Box>
                        </Spacing>
                        <Spacing pt={4}>
                            <Text type='headline' role='black' bold> ХАМГИЙН ИХ ҮЗЭЛТТЭЙ </Text>
                        </Spacing>
                        <Box width='100%' alignItems='center'>
                            <Box height={445} justifyContent='center'>
                                <Spacing pv={4}>
                                    <FlatList
                                        data={cardsData}
                                        renderItem={({ item }) => (
                                            <LessonDashboard
                                                url={item.source}
                                                name={item.title}
                                                desc={item.description}
                                                onPress={() => console.log('ahahaha')}
                                                type='unlocked'
                                                circleButton
                                            />
                                        )}
                                        ItemSeparatorComponent={() => <Box height={16} />}
                                        keyExtractor={(item) => item.id}
                                    />
                                </Spacing>
                            </Box>
                            <Spacing pv={4}>
                                <Button onPress={() => { console.log('aajjajaja') }} size='s' width={343}>
                                    <Box flexDirection='row' justifyContent='center' alignItems='center'>
                                        <Box>
                                            <Text type='callout' role='white' bold={true} fontFamily='Montserrat'>Цааш үзэх</Text>
                                        </Box>
                                        <Box width={40} justifyContent='center' alignItems='center'>
                                            <RightArrowIcon />
                                        </Box>
                                    </Box>
                                </Button>
                            </Spacing>
                        </Box>
                    </Spacing>
                    <Box height={120} />
                </Box>
            </ScrollView>
        </Box>
    )
}