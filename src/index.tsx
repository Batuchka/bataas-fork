import React from 'react';
import { RootNavigationContainer } from './navigation';
import { ThemeProvider } from './components';
import { AuthUserProvider } from '../src/provider/auth';
const App = () => {
  return (
    <AuthUserProvider>
      <ThemeProvider>
        <RootNavigationContainer />
      </ThemeProvider>
    </AuthUserProvider>
  );
};

export default App;
