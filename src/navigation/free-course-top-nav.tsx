import React from 'react'
import { Image, StyleSheet } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { FCAboutScreen, FCStudyScreen } from '../screen/free-course';
import { NavigationContainer } from '@react-navigation/native';
import { MyTab } from './top-nav'

const Tab = createMaterialTopTabNavigator()

export const FreeCourseTopNav = () => {

    return (
        <NavigationContainer>
            <Image source={require('../assets/images/free-course-img.png')} style={styles.image} />
            <Tab.Navigator style={{ flex: 1 }} tabBar={props => <MyTab {...props} />}>
                <Tab.Screen name="Тухай" component={FCAboutScreen} />
                <Tab.Screen name="Хичээлүүд" component={FCStudyScreen} />
            </Tab.Navigator>
        </NavigationContainer>
    )
}
const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 256
    }
})