export enum NavigationRoutes {
  MainRoot = 'MainRoot',
  Home = 'Home',
  SignUp = 'SignUp',
  Course = 'Course',
  Profile = 'Profile',
  ProfileNotLoggedIn = 'ProfileNotLoggedIn',
  Admission = 'Admission',
  PersonalInformation = 'PersonalInformation',
  Test = 'Test',
  Splash = 'Splash',
  HopScreen = 'HopScreen',
  LeapScreen = 'LeapScreen',
  ExamWarning1 = 'ExamWarning1',
  ExamWarning2 = 'ExamWarning2',
  ExamWarning3 = 'ExamWarning3',
  ExamWarning4 = 'ExamWarning4',
  Success = 'Success',
  HomeScreen = 'HomeScreen',
  InternetFailedScreen = 'InternetFailedScreen',
  RegistrationSuccessScreen = 'RegistrationSuccessScreen',
}
export interface NavigationPayload<T> {
  props: T;
}
export type NavigatorParamList = {
  [NavigationRoutes.Splash]: NavigationPayload<any>;
  [NavigationRoutes.SignUp]: NavigationPayload<any>;
  [NavigationRoutes.MainRoot]: NavigationPayload<any>;
  [NavigationRoutes.Home]: NavigationPayload<any>;
  [NavigationRoutes.Course]: NavigationPayload<any>;
  [NavigationRoutes.Profile]: NavigationPayload<any>;
  [NavigationRoutes.ProfileNotLoggedIn]: NavigationPayload<any>;
  [NavigationRoutes.Admission]: NavigationPayload<any>;
  [NavigationRoutes.PersonalInformation]: NavigationPayload<any>;
  [NavigationRoutes.Test]: NavigationPayload<any>;
  [NavigationRoutes.Splash]: NavigationPayload<any>;
  [NavigationRoutes.HopScreen]: NavigationPayload<any>;
  [NavigationRoutes.LeapScreen]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning1]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning2]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning3]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning4]: NavigationPayload<any>;
  [NavigationRoutes.Success]: NavigationPayload<{ successMessage: string }>;
  [NavigationRoutes.HomeScreen]: NavigationPayload<{ HomeScreen: string }>;
  [NavigationRoutes.InternetFailedScreen]: NavigationPayload<{
    description: string,
    title: string,
    icon: any,
    onClick: () => void,
  }>;
  [NavigationRoutes.RegistrationSuccessScreen]: NavigationPayload<{
    onClick: () => void,
  }>;
};
