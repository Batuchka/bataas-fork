import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import {
  TestScreen,
  ExamWarningScreen1,
  ExamWarningScreen2,
  ExamWarningScreen3,
  ExamWarningScreen4,
  CourseScreen,
  ProfileNotLoggedInScreen,
  ProfileScreen,
  AdmissionScreen,
  PersonalInformationScreen,
} from '../screen';
import MainBottomNavigation from './main-bottom-navigation';
import { SplashScreen, InternetFailedScreen, RegistrationSuccessScreen } from '../screen';
import { HopScreen, LeapScreen } from '../screen/main-cources'
import { NavigationRoutes, NavigatorParamList } from './navigation-params';
import { SuccessScreen } from '../screen/success';

const RootStack = createStackNavigator<NavigatorParamList>();

export const RootNavigationContainer = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName={NavigationRoutes.Splash}>
        <RootStack.Screen
          name={NavigationRoutes.Course}
          component={CourseScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.Splash}
          component={SplashScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.ExamWarning1}
          component={ExamWarningScreen1}
          options={{ headerShown: false, gestureEnabled: true }}
        />
        <RootStack.Screen
          name={NavigationRoutes.ExamWarning2}
          component={ExamWarningScreen2}
          options={{ headerShown: false, gestureEnabled: true }}
        />
        <RootStack.Screen
          name={NavigationRoutes.ExamWarning3}
          component={ExamWarningScreen3}
          options={{ headerShown: false, gestureEnabled: true }}
        />
        <RootStack.Screen
          name={NavigationRoutes.ExamWarning4}
          component={ExamWarningScreen4}
          options={{ headerShown: false, gestureEnabled: true }}
        />
        <RootStack.Screen
          name={NavigationRoutes.HopScreen}
          component={HopScreen}
          options={{ headerShown: false, gestureEnabled: true }}
        />
        <RootStack.Screen
          name={NavigationRoutes.LeapScreen}
          component={LeapScreen}
          options={{ headerShown: false, gestureEnabled: true }}
        />
        <RootStack.Screen
          name={NavigationRoutes.MainRoot}
          component={MainBottomNavigation}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.InternetFailedScreen}
          component={InternetFailedScreen}
          options={{
            cardStyle: { backgroundColor: 'transparent' },
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current: { progress } }) => ({
              cardStyle: {
                opacity: progress,
              },
            }),
          }}
        />
        <RootStack.Screen
          name={NavigationRoutes.RegistrationSuccessScreen}
          component={RegistrationSuccessScreen}
          options={{ headerShown: false }} />

        <RootStack.Screen
          name={NavigationRoutes.ProfileNotLoggedIn}
          component={ProfileNotLoggedInScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.Profile}
          component={ProfileScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.Admission}
          component={AdmissionScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name={NavigationRoutes.PersonalInformation}
          component={PersonalInformationScreen}
          options={{ headerShown: false }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
