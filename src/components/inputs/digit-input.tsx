import React, { useRef, useContext, useEffect } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { Box } from '../layout/box';
import { AuthContext } from '../../provider/auth'
const styles = StyleSheet.create({
    digit: {
        width: 40,
        height: 47,
        backgroundColor: '#F2F2F7',
        borderRadius: 4,
        textAlign: 'center',
        marginHorizontal: 10,
    }
})

export const DigitInput: React.FC<any> = () => {
    const { setcode, code } = useContext(AuthContext)
    let refs = [
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
    ]
    const inputRefManage = (event: any, id: number) => {
        if (1 < id && event.nativeEvent.key == 'Backspace') {
            refs[id - 2].current?.focus()
        } else if (event.nativeEvent.key !== 'Backspace' && id < 6) {
            refs[id].current?.focus()
        }
    }
    useEffect(() => {
        if (code.pin6 !== undefined && code.final == undefined) {
            setcode({ ...code, final: `${code.pin1}${code.pin2}${code.pin3}${code.pin4}${code.pin5}${code.pin6}` })
        }
    }, [code])
    return (
        <Box height={56} flexDirection='row' justifyContent='center' alignItems='center'>
            <TextInput ref={refs[0]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setcode({ ...code, pin1: pin }) }} onKeyPress={event => { inputRefManage(event, 1) }} />
            <TextInput ref={refs[1]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setcode({ ...code, pin2: pin }) }} onKeyPress={event => { inputRefManage(event, 2) }} />
            <TextInput ref={refs[2]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setcode({ ...code, pin3: pin }) }} onKeyPress={event => { inputRefManage(event, 3) }} />
            <TextInput ref={refs[3]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setcode({ ...code, pin4: pin }) }} onKeyPress={event => { inputRefManage(event, 4) }} />
            <TextInput ref={refs[4]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setcode({ ...code, pin5: pin }) }} onKeyPress={event => { inputRefManage(event, 5) }} />
            <TextInput ref={refs[5]} style={[styles.digit]} keyboardType='numeric' placeholder='0' maxLength={1} onChangeText={pin => { setcode({ ...code, pin6: pin }) }} onKeyPress={event => { inputRefManage(event, 6) }} />
        </Box>
    );
};
