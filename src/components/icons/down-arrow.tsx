import React from 'react'
import Svg, { Path } from 'react-native-svg'
import { IconType } from '../types'

type DownArrow = {
    width: string,
    height: string
}

export const ArrowDown:React.FC<DownArrow> = ({width, height}) => {

    return (
        <Svg width={width} height={height} viewBox="0 0 12 6" fill="none">
            <Path d="M6 5.66675L0.5 0.166748H11.5L6 5.66675Z" fill="#8F9CB3" />
        </Svg>

    )
}