import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const CameraIcon: React.FC<IconType> = ({ width = 32, height = 32, role = 'white' }) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none">
      <Path
        d="M13.104 6.667l-2.667 2.666H5.333v16h21.334v-16h-5.105l-2.666-2.666h-5.792zM12 4h8l2.666 2.667H28A1.333 1.333 0 0129.333 8v18.667A1.333 1.333 0 0128 28H4a1.334 1.334 0 01-1.333-1.333V8A1.333 1.333 0 014 6.667h5.333L12 4zm4 20a7.333 7.333 0 110-14.666A7.333 7.333 0 0116 24zm0-2.667A4.667 4.667 0 1016 12a4.667 4.667 0 000 9.333z"
        fill={colors[role]}
      />
    </Svg>
  )
}