import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const LeftArrowIcon: React.FC<IconType> = ({
  role = 'black',
  width = 24,
  height = 25,
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={24} height={25} viewBox="0 0 24 25" fill="none">
      <Path
        d="M10.828 12.005l4.95 4.95-1.414 1.414L8 12.005l6.364-6.364 1.414 1.414-4.95 4.95z"
        fill={colors[role]}
      />
    </Svg>
  );
};
