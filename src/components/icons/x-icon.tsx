import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
import {IconType} from '../types';

export const XIcon: React.FC<IconType> = ({
  role = 'primary500',
  width = 32,
  height = 32,
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 32 32"
      fill="none"
    >
      <Path
        d="M24 8L8 24M8 8l16 16"
        stroke="#172B4D"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
