import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { IconType } from "../types"

export const UserIcon:React.FC<IconType> = ({ width= 14, height = 19}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 14 19"
      fill="none"
    >
      <Path
        d="M13.667 18.333H12v-1.667a2.5 2.5 0 00-2.5-2.5h-5a2.5 2.5 0 00-2.5 2.5v1.667H.333v-1.667A4.167 4.167 0 014.5 12.5h5a4.167 4.167 0 014.167 4.166v1.667zM7 10.833A5 5 0 117 .834a5 5 0 010 9.999zm0-1.667A3.333 3.333 0 107 2.5a3.333 3.333 0 000 6.666z"
        fill="#172B4D"
      />
    </Svg>
  )
}
