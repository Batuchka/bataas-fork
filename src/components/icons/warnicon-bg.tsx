import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"
import { IconType } from '../types'

export const WarnIconBG:React.FC<IconType> = ({width, height}) => {
  return (
    <Svg
      width={width || 64}
      height={height || 64}
      viewBox="0 0 64 64"
      fill="none"
    >
      <Circle cx={32} cy={32} r={32} fill="#FEEA8A" />
      <Path
        d="M43 13l11 19-11 19H21L10 32l11-19h22zm-2.306 4H23.306l-8.684 15 8.684 15h17.388l8.684-15-8.684-15zM30 38h4v4h-4v-4zm0-16h4v12h-4V22z"
        fill="#9C6F19"
      />
    </Svg>
  )
}

