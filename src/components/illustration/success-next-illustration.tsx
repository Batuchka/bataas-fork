import React from "react"
import Svg, { Circle, G, Path, Pattern, Image, Defs, Use, ClipPath } from "react-native-svg"

export const SuccessNextIllustration: React.FC<any> = (props) => {
    return (
        <Svg
        width={48}
        height={48}
        viewBox="0 0 48 48"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        <Path
          d="M0 24C0 10.745 10.745 0 24 0s24 10.745 24 24-10.745 24-24 24S0 37.255 0 24z"
          fill="#172B4D"
        />
        <G clipPath="url(#prefix__clip0)">
          <Path
            d="M28.172 23l-5.364-5.364 1.414-1.414L32 24l-7.778 7.778-1.414-1.414L28.172 25H16v-2h12.172z"
            fill="#fff"
          />
        </G>
        <Path
          d="M24 47C11.297 47 1 36.703 1 24h-2c0 13.807 11.193 25 25 25v-2zm23-23c0 12.703-10.297 23-23 23v2c13.807 0 25-11.193 25-25h-2zM24 1c12.703 0 23 10.297 23 23h2C49 10.193 37.807-1 24-1v2zm0-2C10.193-1-1 10.193-1 24h2C1 11.297 11.297 1 24 1v-2z"
          fill="#fff"
        />
        <Defs>
          <ClipPath id="prefix__clip0">
            <Path fill="#fff" transform="translate(12 12)" d="M0 0h24v24H0z" />
          </ClipPath>
        </Defs>
      </Svg>
    )
}