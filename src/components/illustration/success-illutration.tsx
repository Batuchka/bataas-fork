import React from "react"
import Svg, { Circle, Path, Pattern, Image, Defs, Use } from "react-native-svg"

export const SuccessIllustration: React.FC<any> = (props) => {
    return (
        <Svg
            width={204}
            height={204}
            viewBox="0 0 204 204"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Circle opacity={0.03} cx={102} cy={102} r={102} fill="#172B4D" />
            <Circle opacity={0.03} cx={102} cy={102} r={86} fill="#172B4D" />
            <Circle opacity={0.03} cx={102} cy={102} r={70} fill="#172B4D" />
            <Path
                d="M102 48c-29.777 0-54 24.223-54 54s24.223 54 54 54 54-24.223 54-54-24.223-54-54-54z"
                fill="#172B4D"
            />
            <Path
                d="M129.369 90.557l-29.25 29.249a4.486 4.486 0 01-3.181 1.319 4.486 4.486 0 01-3.182-1.319l-14.624-14.625a4.493 4.493 0 010-6.362 4.494 4.494 0 016.362 0l11.444 11.443 26.069-26.068a4.493 4.493 0 016.362 0 4.496 4.496 0 010 6.363z"
                fill="#FAFAFA"
            />
        </Svg>
    )
}


  