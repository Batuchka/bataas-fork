import React, { useState, createContext, useContext } from 'react'
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native'
import { Border, Text } from './core'
import { Box, Queue, Spacing, Stack } from './layout'
import { ArrowDown } from './icons'
import { set } from 'react-native-reanimated'

const DropDownContext = createContext({
    visible: false,
    setVisible: (visible: boolean) => { },
    chosenOne: '',
    setChosenOne: (chosenOne: string) => { }
});

export const DropDown = {

    Provider: ({ children }: any) => {
        const [visible, setVisible] = useState<boolean>(false);
        const [chosenOne, setChosenOne] = useState<string>('')
        return (
            <DropDownContext.Provider value={{ visible, setVisible, chosenOne, setChosenOne }}>
                {children}
            </DropDownContext.Provider>
        );
    },
    Trigger: ({ children, title }: any) => {
        const { visible, setVisible, chosenOne, setChosenOne } = useContext(DropDownContext);

        return (
            <TouchableOpacity onPress={() => setVisible(!visible)}>
                <Border radius={4} role={'primary500'} lineWidth={2}>
                    <Box height={56} role={'primary100'}>
                        <Spacing pl={4} pr={4} pt={2} pb={2}>
                            <Box flexDirection={'row'} width={'100%'} role={'primary300'} height={'100%'} alignItems={'center'} justifyContent={'space-between'}>
                                <Stack size={2}>
                                    <Queue>
                                    {
                                        chosenOne !== '' ?
                                            <Text type={'caption1'} role={'primary400'}>{title}</Text>
                                            :
                                            <Text width = {'auto'} type={'body'} role={'primary400'}>{title}</Text>
                                    }
                                    <Text role={'destructive500'} type={'caption1'}>*</Text>
                                    </Queue>
                                    {
                                        chosenOne !== '' ?
                                            <Text type={'body'} role={'black'}>{chosenOne}</Text>
                                            :
                                            <></>
                                    }
                                </Stack>
                                <ArrowDown width={'12'} height={'6'} />
                            </Box>
                        </Spacing>
                    </Box>
                </Border>
            </TouchableOpacity>
        )
    },
    Content: ({ children, contents }: any) => {
        const { visible, setVisible, chosenOne, setChosenOne } = useContext(DropDownContext);
        return (
            <>
                {
                    visible ?
                        React.Children.toArray(children).map((child, i) => {
                            const func = () => {
                                setVisible(!visible)
                                setChosenOne(contents[i])
                            }
                            return (
                                <TouchableOpacity onPress={() => func()}>
                                    <Border topWidth={0} lineWidth={1} role={'black'} key={i}>
                                        <Box height={56} role={'white'} justifyContent={'center'}>
                                            <Spacing pl={4}>
                                                {child}
                                            </Spacing>
                                        </Box>
                                    </Border>
                                </TouchableOpacity>
                            )
                        })
                        :
                        <></>
                }
            </>
        )
    }
}
