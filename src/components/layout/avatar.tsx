import React from 'react';
import { Image, StyleSheet, Text } from 'react-native';
import { Box, Border } from '../../components';

const sizeDimensions = { L: 128, M: 64, S: 40, XS: 24 };
const textDimensions = { L: 34, M: 22, S: 12, XS: 8 };

type AvatarType = {
  initial?: string;
  url?: string;
  size: 'L' | 'M' | 'S' | 'XS';
};

export const Avatar: React.FC<AvatarType> = ({ initial = 'ДЭ', url, size }) => {
  const styles = StyleSheet.create({
    image: {
      width: sizeDimensions[size],
      height: sizeDimensions[size],
    },
    text: {
      fontStyle: 'normal',
      fontWeight: 'bold',
      fontSize: textDimensions[size],
    },
  });

  if (size === 'XS')
    return (
      <Box width={sizeDimensions[size]} height={sizeDimensions[size]}>
        <Border radius={sizeDimensions[size] / 2}>
          <Image style={styles.image} source={{ uri: url }} />
        </Border>
      </Box>
    );

  return (
    <Box width={sizeDimensions[size]} height={sizeDimensions[size]}>
      <Border radius={sizeDimensions[size] / 2} role="primary200" lineWidth={4}>
        <Box
          width={sizeDimensions[size] - 8}
          height={sizeDimensions[size] - 8}
          role="accentNest"
          justifyContent="center"
          alignItems="center"
        >
          {url ? (
            <Image style={styles.image} source={{ uri: url }} />
          ) : (
            <Text style={styles.text}>{initial}</Text>
          )}
        </Box>
      </Border>
    </Box>
  );
};
