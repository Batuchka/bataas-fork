
import React from 'react';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { UserIcon, BookOpenIcon, ChatIcon, PencilIcon } from '../../components/icons'
import { Text } from '../../components';
import { Border } from './border';

export const ImplementAdmissionProcess = () => {

    return (
        <Spacing ml={1}>
            <Queue justifyContent={'space-between'}>
                <Box alignItems={'center'}>
                    <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                        <Border radius={64} lineWidth={1.5} role={'black'}>
                            <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                                <UserIcon />
                            </Box>
                        </Border>
                    </Box>
                    <Box role={'black'} width={1.5} height={41} />
                    <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                        <Border radius={64} lineWidth={1.5} role={'black'}>
                            <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                                <BookOpenIcon />
                            </Box>
                        </Border>
                    </Box>
                    <Box role={'black'} width={1.5} height={62} />
                    <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                        <Border radius={64} lineWidth={1.5} role={'black'}>
                            <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                                <ChatIcon />
                            </Box>
                        </Border>
                    </Box>
                    <Box role={'black'} width={1.5} height={62} />
                    <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                        <Border radius={64} lineWidth={1.5} role={'black'}>
                            <Box width={32} height={32} justifyContent={'center'} alignItems={'center'}>
                                <PencilIcon />
                            </Box>
                        </Border>
                    </Box>
                </Box>
                <Box width={295}>
                    <Stack size={6}>
                        <Stack size={1.5}>
                            <Text bold type={'body'} role={'black'}>Бүртгэл</Text>
                            <Text type={'callout'} role={'black'}>Эхний алхам бол бүртгүүлэх байгаа</Text>
                        </Stack>
                        <Stack size={1.5}>
                            <Text bold type={'body'} role={'black'}>Шалгалт</Text>
                            <Text type={'callout'} role={'black'}>Бүртгүүлсэний дараа 30 асуулттай шалгалт өгнө</Text>
                        </Stack>
                        <Stack size={1.5}>
                            <Text bold type={'body'} role={'black'}>Ярилцлага</Text>
                            <Text type={'callout'} role={'black'}>Шалгалтандаа тэнцвэл манай мастеруудтай ярилцлаганд орно</Text>
                        </Stack>
                        <Stack size={1.5}>
                            <Text bold type={'body'} role={'black'}>Гэрээ</Text>
                            <Text type={'callout'} role={'black'}>Тэнцвэл ирж гэрээгээ зурж нэст академид сурах боломжтой</Text>
                        </Stack>
                    </Stack>
                </Box>
            </Queue>
        </Spacing>
    );

};