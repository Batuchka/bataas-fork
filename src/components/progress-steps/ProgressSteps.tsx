import React, { createContext, useState } from 'react';
import { Box } from '../';
import { BottomButtons } from './bottomButtons';
import { Header } from './header';

interface ProgressStepsProps {
   lastButtonFunction: Function;
   children?: any;
}

export const progressStepsContext = createContext<any>(null);

export const ProgressSteps: React.FC<ProgressStepsProps> = ({ lastButtonFunction, children }) => {
   const childrens: any = React.Children.toArray(children);
   const [currentPageIndex, setCurrentPageIndex] = useState<any>(0);
   
   return (
      <progressStepsContext.Provider value={{ childrens, currentPageIndex, setCurrentPageIndex, lastButtonFunction }}>
         <Box width={'100%'} height={'100%'} alignItems={'center'}>
            <Header />
            {childrens[currentPageIndex]}
            <BottomButtons />
         </Box>
      </progressStepsContext.Provider>
   )
}