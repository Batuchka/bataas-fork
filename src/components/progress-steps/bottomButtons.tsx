import React, { useContext } from 'react';
import { Dimensions, View } from 'react-native';
import { progressStepsContext } from './ProgressSteps';
import { Box, Spacing, Button, Text, ArrowIcon } from '../';
const { width } = Dimensions.get('window');

interface getScreenNameProps {
   array: any;
   index: number;
   jumpTo: 'next' | 'previous';
}

export const BottomButtons: React.FC<any> = () => {
   const { childrens, currentPageIndex, setCurrentPageIndex, lastButtonFunction } = useContext(progressStepsContext);

   const onClick = async ({ array, index, jumpTo }: getScreenNameProps) => {
      const length = array.length - 1;
      const { onNext, catchFunction } = array[index].props;

      switch (jumpTo) {
         case 'next':
            if (index === length)
               lastButtonFunction()
            else {
               try {
                  await onNext && onNext()
                  setCurrentPageIndex(index + 1)
               } catch { () => catchFunction() }
            }
            break;

         case 'previous':
            if (index !== 0)
               setCurrentPageIndex(index - 1)
            break;
      }
   }

   if (currentPageIndex === 0)
      return (
         <Box width={width * 0.9} position={'absolute'} bottom={10}>
            <Button onPress={() => onClick({ array: childrens, index: currentPageIndex, jumpTo: 'next' })} category={'fill'} size={'l'} width={'100%'}>
               <Box flexDirection={'row'} alignItems={'center'}>
                  <Text type={'body'} fontFamily={'Montserrat'} bold role={'white'} width={'auto'}>Дараах</Text>
                  <Spacing ml={1}>
                     <View style={{ transform: [{ rotate: '-90deg' }] }}>
                        <ArrowIcon color={'white'} height={6} width={12} />
                     </View>
                  </Spacing>
               </Box>
            </Button>
         </Box>)
   else
      return (
         <Box width={width * 0.9} position={'absolute'} bottom={10} flexDirection={'row'} justifyContent={'space-between'}>
            <Button onPress={() => onClick({ array: childrens, index: currentPageIndex, jumpTo: 'previous' })} category={'ghost'} size={'l'} width={width * 0.425}>
               <Text type={'body'} fontFamily={'Montserrat'} bold role={'primary500'}>Буцах</Text>
            </Button>
            <Button onPress={() => onClick({ array: childrens, index: currentPageIndex, jumpTo: 'next' })} category={'fill'} size={'l'} width={width * 0.425}>
               <Box flexDirection={'row'} alignItems={'center'}>
                  <Text type={'body'} fontFamily={'Montserrat'} bold role={'white'} width={'auto'}>Дараах</Text>
                  <Spacing ml={1}>
                     <View style={{ transform: [{ rotate: '-90deg' }] }}>
                        <ArrowIcon color={'white'} height={6} width={12} />
                     </View>
                  </Spacing>
               </Box>
            </Button>
         </Box>)
}