import React, { useContext } from 'react';
import { Dimensions, View } from 'react-native';
import { progressStepsContext } from './ProgressSteps';
import { Box, Button, Text, ArrowIcon } from '../';
import { ProgressBar } from '../progress-bar';
import { useNavigation } from '@react-navigation/core';
const { width } = Dimensions.get('window');

export const Header: React.FC<any> = () => {
   const { childrens, currentPageIndex, setCurrentPageIndex } = useContext(progressStepsContext);
   const navigation = useNavigation();
   const progressLength = ((100) / childrens.length) * (currentPageIndex + 1);

   const back = () => {
      if (currentPageIndex !== 0)
         setCurrentPageIndex(currentPageIndex - 1)
      else
         navigation.goBack();
   }

   return (
      <Box width={width} height={54}>
         <Box flexDirection={'row'} alignItems={'center'} justifyContent={'center'} width={width} height={50}>
            <Box position={'absolute'} left={30}>
               <Button category={'text'} onPress={() => back()}>
                  <View style={{ transform: [{ rotate: '90deg' }] }}>
                     <ArrowIcon height={10} width={16} color={'black'} />
                  </View>
               </Button>
            </Box>
            <Text type={'headline'} fontFamily={'Montserrat'} role={'black'} bold width={'auto'}>Бүртгэл</Text>
         </Box>
         <Box width={width} height={5} role={'primary300'}>
            <ProgressBar role={'success400'} endProgress={progressLength} duration={1000} />
         </Box>
      </Box>
   )
}