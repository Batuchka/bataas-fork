import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Text } from '../core';
import { ProgressStep } from './ProgressStep';
import { ProgressSteps } from './ProgressSteps';

export const TestProgress = () => {
   return (
      <SafeAreaView style={{ flex: 1 }}>
         <ProgressSteps lastButtonFunction={() => console.log('jump')}>
            <ProgressStep label={'step-1'} id={'step-1'} onNext={() => console.log('onNext')} catchFunction={() => console.log('error')}>
               <Text>page1</Text>
            </ProgressStep>
            <ProgressStep label={'step-2'} id={'step-2'}>
               <Text>page2</Text>
            </ProgressStep>
            <ProgressStep label={'step-3'} id={'step-3'}>
               <Text>page3</Text>
            </ProgressStep>
         </ProgressSteps>
      </SafeAreaView>
   )
}