import React from 'react';

interface Props {
   label: string,
   id: string,
   onNext?: Function,
   catchFunction?: Function,
   children?: any
}

export const ProgressStep: React.FC<Props> = ({ label, id, children }) => {
   return children
}