import React from 'react'
import { Share, Alert } from 'react-native'
import { Button, Text } from './core'


export const ShareButton = () => {
    const onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    'React Native | A framework for building native apps using React',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            Alert.alert(error.message);
        }
    };
    return (
        <Button onPress={onShare} size="s">
            <Text
                fontFamily={'Montserrat'}
                role={'white'}
                type={'callout'}
                textAlign={'center'}
                bold
            >
                Share
            </Text>
        </Button>
    )
}
