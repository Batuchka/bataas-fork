import React, { createContext, useContext, useEffect, useState } from 'react';
import { Border, Button } from './core';
import { Box, Queue, Spacing, Stack } from './layout';
import _ from 'lodash';
import { TouchableOpacity } from 'react-native-gesture-handler';

type GroupCheckboxType = {
  horizontal?: boolean;
  children: any;
  defaultValue?: number,
  size?: number
};

type GroupCheckboxItemType = {
  checkbox?: boolean;
  children?: any;
  index: number;
};

export const CheckBoxContext = createContext({
  checked: null,
  setChecked: () => { },
  horizontal: null
})

export const GroupCheckBox: React.FC<GroupCheckboxType> = ({
  horizontal,
  children,
  defaultValue,
  size = 4
}) => {
  const [checked, setChecked]: any = useState(defaultValue ? defaultValue : null)

  return (
    <CheckBoxContext.Provider value={{ checked, setChecked, horizontal }}>
      <Box flexDirection={horizontal ? 'column' : 'row'} width="100%">
        {horizontal ? (
          <Stack size={size} width={'100%'}>
            {children}
          </Stack>
        ) : (
          <Queue size={size} justifyContent="space-around">{children}</Queue>
        )}
      </Box>
    </CheckBoxContext.Provider>
  );
};

export const CheckBoxItem: React.FC<GroupCheckboxItemType> = ({
  children,
  checkbox,
  index,
}) => {
  const { setChecked: set, checked: check, horizontal } = useContext(CheckBoxContext)
  const [checked, setChecked] = useState(false)

  useEffect(() => {
    check === index ? setChecked(true) : setChecked(false)
  }, [check])

  return (
    <TouchableOpacity onPress={() => check === index ? set(null) : set(index)}>
      <Border lineWidth={1} role={checked ? 'success400' : "gray"} radius={8}>
        {(checkbox && !horizontal) &&
          <Box position='absolute' top={10} right={10}>
            <Border lineWidth={2} role={checked ? 'success400' : "gray"} radius={20}>
              <Box width={20} height={20}></Box>
            </Border>
          </Box>
        }
        <Box flexDirection={horizontal ? 'row' : 'column'} alignItems='center'>
          {children}
          {(checkbox && horizontal) && (
            <Border lineWidth={2} role={checked ? 'success400' : "gray"} radius={20}>
              <Box width={20} height={20}></Box>
            </Border>
          )}
        </Box>
      </Border>
    </TouchableOpacity>
  );
};
