
import React, { createContext, useEffect, useState } from 'react';
import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'

export const AuthContext = createContext<{
    user?: FirebaseAuthTypes.User;
    confirmCode: () => {};
    signInWithPhoneNumber: () => {};
    signOut: () => void;
    code: any;
    number: any,
    setcode: Function
    setnumber: Function
}>({
    user: null,
    code: null,
    number: null,
    confirmCode: () => {},
    signInWithPhoneNumber: () => { },
    signOut: () => { },
    setcode: () => { },
});

export const AuthUserProvider = ({ children }: any) => {
    const [user, setuser] = useState<FirebaseAuthTypes.User>(null);
    const [confirm, setConfirm] = useState(null);
    const [number, setnumber] = useState<string>('');
    const [code, setcode] = useState('')
    useEffect(() => {
        auth().onAuthStateChanged(async (user) => {
            console.log('Working!');
            if (!user) {
                return;
            }
            setuser(user);
        });
    }, []);

    const signInWithPhoneNumber = async () => {
        const confirmation = await auth().signInWithPhoneNumber(`+976${number}`);
        setConfirm(confirmation);
    };
    const confirmCode = async () => {
        console.log('confirm')
        await confirm.confirm(code.final);
    };
    const signOut = () => {
        auth()
            .signOut()
            .then(() => console.log('User signed out!'));
    };
    return (
        <AuthContext.Provider value={{
            user,
            code,
            number,
            setnumber,
            confirmCode,
            signInWithPhoneNumber,
            signOut,
            setcode,
        }}>
            {children}
        </AuthContext.Provider>
    );
};
